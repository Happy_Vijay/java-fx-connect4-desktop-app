package Connect4;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {
	private Controller controller;

	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("game.fxml"));
		GridPane rootGridPane = loader.load();
		controller = loader.getController();
		controller.createPlayground();
		Pane myPane = (Pane) rootGridPane.getChildren().get(0);
		MenuBar menuBar = createMenu();
		menuBar.prefWidthProperty().bind(primaryStage.widthProperty());
		myPane.getChildren().add(menuBar);
		Scene scene = new Scene(rootGridPane);
		primaryStage.setTitle("Connect4");
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.show();
	}

	private MenuBar createMenu() {
		//File Menu
		Menu fileMenu = new Menu("File");
		//add menu Items
		MenuItem newGame = new MenuItem("New Game");

		MenuItem resetGame = new MenuItem("Reset Game");
		SeparatorMenuItem separatorMenuItem = new SeparatorMenuItem();
		MenuItem exitGame = new MenuItem("Exit Game");
		fileMenu.getItems().addAll(newGame, resetGame, separatorMenuItem, exitGame);
		newGame.setOnAction(event -> {
			//code for new Game
			controller.resetGame();
		});
		resetGame.setOnAction(event -> controller.resetGame());
		exitGame.setOnAction(event -> exitGame());
		//Help Menu
		Menu helpMenu = new Menu("Help");
		//Add MenuItems
		MenuItem aboutConnect4 = new MenuItem("About Connect4");
		aboutConnect4.setOnAction(event -> aboutConnect4());

		helpMenu.getItems().addAll(aboutConnect4);
		MenuBar menuBar = new MenuBar();
		menuBar.getMenus().addAll(fileMenu, helpMenu);
		return menuBar;
	}

	private void aboutConnect4() {
		Alert aboutGame = new Alert(Alert.AlertType.INFORMATION);
		aboutGame.setTitle("About Connect4");
		aboutGame.setContentText("Connect Four is a two-player connection game in which the players first choose a color and then take turns dropping colored discs from the top into a seven-column, six-row vertically suspended grid The pieces fall straight down, occupying the next available space within the column. The objective of the game is to be the first to form a horizontal, vertical, or diagonal line of four of one's own discs. Connect Four is a solved game. The first player can always win by playing the right moves.");
		aboutGame.show();
	}

	private void exitGame() {
		Platform.exit();
		System.exit(0);
	}




	public static void main(String[] args) {
		launch(args);
	}
}
