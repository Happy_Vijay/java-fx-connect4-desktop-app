Connect Four is a two-player connection game in which the players first choose a color and then take turns dropping colored discs
from the top into a seven-column, six-row vertically suspended grid The pieces fall straight down,
occupying the next available space within the column. 
The objective of the game is to be the first to form a horizontal, vertical, or diagonal line of four of one's own discs. 
Connect Four is a solved game.
The first player can always win by playing the right moves.

The steps required to create the project are:

1) Design and layout of App. (Using FXML Scene Builder Tool in Intellij IDEA).
2) Adding click-events, functionalities.
These include:
1) Setname() from two players.
2) Dropping a ring creating Y Translate animations.
3) Deciding if either of the user has won.