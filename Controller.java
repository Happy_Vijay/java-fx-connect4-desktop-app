package Connect4;

import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Controller implements Initializable {
	//Game Rules
	private static final int ROWS= 6;
	private static final int COLUMNS = 7;
	private static final String diskOne= "24303E";
	private static final String diskTwo= "4CAA88";
	private static final int disksize= 80;
	//player names
	private static String playerOne= "Player 1";
	private static String playerTwo = "Player 2";
	//To toggle turns
	private boolean isPlayerOneTurn= true;
	@FXML
	GridPane rootGridPane;
	@FXML
	Pane insertDiskPane;
	@FXML
	Label playerName;
	@FXML
	Button setName;
	@FXML
	TextField player1Text;
	@FXML
	TextField player2Text;
	private boolean isInsertedDiscover=true;
	Disc [][]insertedDiscArray = new Disc[ROWS][COLUMNS];
	public void createPlayground(){
		Shape rectangleWithHoles = createRectangleWithHoles();
	List <Rectangle> rectangleList= createRectangle();

		rootGridPane.add(rectangleWithHoles,0,1);
		for (Rectangle rectangle: rectangleList
		     ) {

			rootGridPane.add(rectangle,0,1);
		}

	}

	private Shape createRectangleWithHoles()
	{
		Shape rectangleWithHoles = new Rectangle((COLUMNS+1)*disksize,(ROWS+1)*disksize);

		for (int i = 0; i <   ROWS; i++) {
			for (int j = 0; j <COLUMNS ; j++) {

				Circle circle = new Circle();
				circle.setRadius(disksize/2);
				circle.setCenterY(disksize/2);
				circle.setCenterX(disksize/2);
				circle.setSmooth(true);
				circle.setTranslateY(i*(disksize+5)+disksize/4);
				circle.setTranslateX(j*(disksize+5)+disksize/4);
				rectangleWithHoles=  Shape.subtract(rectangleWithHoles,circle);
			}
		}

		rectangleWithHoles.setFill(Color.WHITE);
		return  rectangleWithHoles;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		setName.setOnAction(event -> {
			playerOne=player1Text.getText();
			playerTwo=player2Text.getText();
		});
	}
	private static class Disc extends  Circle{
		private final boolean  isPlayerOneMove;

		private Disc(boolean isPlayerOneMove) {
			this.isPlayerOneMove = isPlayerOneMove;
			setRadius(disksize/2);
			setCenterX(disksize/2);
			setCenterY(disksize/2);
			setFill(isPlayerOneMove? Color.valueOf(diskOne) : Color.valueOf(diskTwo));
		}
	}
	public List<Rectangle> createRectangle()

	{
		List<Rectangle> rectangleList= new ArrayList();
		for (int i = 0; i <COLUMNS ; i++) {
			Rectangle rectangle = new Rectangle(disksize,(ROWS+1)*disksize);
			rectangle.setFill(Color.TRANSPARENT);
			rectangle.setTranslateX((i)*(disksize+5)+disksize/4);
			rectangle.setOnMouseEntered(event -> rectangle.setFill(Color.valueOf("#eeeeee26")));
			rectangle.setOnMouseExited(event -> rectangle.setFill(Color.TRANSPARENT));
			 final int column=i;
			rectangle.setOnMouseClicked(event -> {
				if(isInsertedDiscover) {
					isInsertedDiscover=false;
					insertDisc(new Disc(isPlayerOneTurn), column);
				}
			});
			rectangleList.add(rectangle);
		}

		return rectangleList;
	}

	private void insertDisc(Disc disc,int column)
	{   int rows=ROWS-1;
		while(rows>=0)
		{
			if(insertedDiscArray[rows][column]==null)
				break;
			else
			rows--;
		}
		if(rows<0)
			return;
			//Structural Changes
		insertedDiscArray[rows][column]= disc;
		//Visual Change
		insertDiskPane.getChildren().add(disc);
		disc.setTranslateX(column*(disksize+5)+disksize/4);
		TranslateTransition translateTransition= new TranslateTransition(Duration.seconds(0.5),disc);
	translateTransition.setToY(rows*(disksize+5)+disksize/4);
	translateTransition.play();
	int currentRow = rows;
	translateTransition.setOnFinished(event -> {
		isInsertedDiscover=true;
		if(gameEnded(currentRow,column))
		{
			gameWinner();
		}
		isPlayerOneTurn=!isPlayerOneTurn;
		playerName.setText(isPlayerOneTurn? playerOne:playerTwo);
	});
	}

	private void gameWinner()

	{
		String winner = isPlayerOneTurn ? playerOne : playerTwo;
		Alert declareWinner = new Alert(Alert.AlertType.INFORMATION);
		declareWinner.setTitle("Connect4");
		declareWinner.setHeaderText("The winner is: "+ winner);
		declareWinner.setContentText("Do you wish to play another Game");
		ButtonType yesBtn = new ButtonType("Yes");
		ButtonType noBtn = new ButtonType("No,exit game");
		declareWinner.getButtonTypes().setAll(yesBtn,noBtn);
	Platform.runLater(()-> {
		Optional<ButtonType> clickedButton = declareWinner.showAndWait();
		if (clickedButton.isPresent() && clickedButton.get() == yesBtn) {
			//Code to reset the game
			resetGame();
		} else {
			//code to exit the game

			Platform.exit();
			System.exit(0);
		}
	});
	}

	public void resetGame() {
		//Structurally reset game
		isPlayerOneTurn=true;
		playerName.setText(playerOne);
		for (int i = 0; i <ROWS ; i++) {
			for (int j = 0; j <COLUMNS ; j++) {
				insertedDiscArray[i][j]=null;

			}

		}
		//Visually Reset Game
		insertDiskPane.getChildren().clear();
		createPlayground();
	}


	private boolean gameEnded(int rows,int columns)
{
	//Vertical points, Horizontal Points, diagonal Points
	// Vertical Points
	List <Point2D> verticalPoints = IntStream.rangeClosed(rows-3,rows+3)
			.mapToObj(r->new Point2D(r,columns)).collect(Collectors.toList());
	//Horizontal Points
	List <Point2D> horizontalPoints = IntStream.rangeClosed(columns-3,columns+3)
			.mapToObj(c->new Point2D(rows,c))
			.collect(Collectors.toList());
	Point2D startPoint1 = new Point2D((rows-3),(columns-3));
	List<Point2D> diagonal1Points = IntStream.rangeClosed(0,6).mapToObj(i->startPoint1.add(i,i))
			.collect(Collectors.toList());

	Point2D startPoint2 = new Point2D((rows-3),(columns+3));
	List<Point2D> diagonal2Points =IntStream.rangeClosed(0,6)
			.mapToObj(j->startPoint2.add(j,-j))
			.collect(Collectors.toList());
	boolean endGame= checkCombination(verticalPoints )  || checkCombination(horizontalPoints) ||
			checkCombination(diagonal1Points) || checkCombination(diagonal2Points);
	return endGame;
}

	private boolean checkCombination(List<Point2D> verticalPoints) {
		int chain=0;
		int rowIndexForArray;
		int columnIndexForArray;
		for (Point2D point:verticalPoints
		     ) {
		rowIndexForArray= (int) point.getX();
		columnIndexForArray= (int) point.getY();
		Disc disc = getDisc(rowIndexForArray,columnIndexForArray);
		if(disc!=null && disc.isPlayerOneMove==isPlayerOneTurn) {
			chain++;
			if (chain == 4)
				return true;
		}
		else chain=0;

		}
		return false;
	}

	private Disc getDisc(int rowIndexForArray, int columnIndexForArray) {
	if(rowIndexForArray>=ROWS  || rowIndexForArray<0 || columnIndexForArray<0 ||columnIndexForArray>=COLUMNS)
		return null;
	else
		return insertedDiscArray[rowIndexForArray][columnIndexForArray];
	}
}
